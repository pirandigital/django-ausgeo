import json
from django.shortcuts import *
from models import *


def type_ahead(request):
    query = request.GET.get('q')
    locations = Location.objects.filter(name__icontains=query).order_by('-rank')[:5]

    datum = [{'value': '{0}, {1}'.format(loc.name, loc.state), 'tokens': loc.name.split()}
             for loc in locations]

    return HttpResponse(json.dumps(datum))