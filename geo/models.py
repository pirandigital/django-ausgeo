from django.contrib.gis.db import models
from django.contrib.gis.db.models.manager import GeoManager
from localflavor.au.models import AUStateField, AUPostCodeField
from location_field.models import LocationField
from autoslug import AutoSlugField
from select2.fields import ForeignKey


class Country(models.Model):
    name = models.CharField(max_length=255)
    code = models.CharField(max_length=2, unique=True)

    class Meta:
        verbose_name_plural = 'countries'

    def __unicode__(self):
        return self.name


class ChoicesManager(models.Manager):

    def as_choices(self):
        yield ('', 'Choose a city ...')
        for obj in self.all().order_by('name'):
            yield (obj.pk, unicode(obj))


class City(models.Model):
    """
    Major population centers.
    """
    name = models.CharField(max_length=255)
    slug = AutoSlugField(max_length=50, unique=True, populate_from='name')
    location = ForeignKey('geo.Location', null=True, blank=True, related_name='+',
                          ajax=True,
                          search_field='keywords',
                          overlay="Choose an location...",
                          js_options={
                              'quiet_millis': 200,
                          })

    objects = ChoicesManager()

    class Meta:
        verbose_name_plural = 'cities'

    def __unicode__(self):
        return self.name


class LocationManager(models.GeoManager):

    def get_first_location_by_postcode(self, postcode):
        return self.filter(postcode=postcode).first()


class Location(models.Model):
    country = models.ForeignKey(Country, related_name='locations')
    city = models.ForeignKey(City, related_name='locations', blank=True, null=True)
    name = models.CharField(max_length=255)
    state = AUStateField()
    postcode = AUPostCodeField()
    keywords = models.CharField(max_length=1024, blank=True)
    point = LocationField(null=True, blank=True, based_fields=[name, state], zoom=8, suffix='Australia')
    rank = models.IntegerField(db_index=True, default=0)
    objects = LocationManager()

    class Meta:
        ordering = ('name',)

    def __unicode__(self):
        return '{0}, {1}'.format(self.name, self.state)
