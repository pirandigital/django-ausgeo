from django.core.management.base import BaseCommand
from geo.fixtures.locations import load_locations


class Command(BaseCommand):
    help = 'Load the locations'

    def handle(self, *args, **options):
        self.stdout.write('Loading location data...')
        return load_locations()
