from geopy import geocoders
from geopy.exc import GeocoderQueryError
from django.contrib.gis.geos import Point
from django import forms

GEO_FIELDS = ('street', 'suburb', 'postcode')


class GeoFormMixin(object):
    """
    Save the location (.point) for the edited entry based on the GEO_FIELDS
    """

    # TODO inject geocoder and mock it for tests

    def clean(self):
        data = super(GeoFormMixin, self).clean()
        address = u' '.join([ data[f] for f in GEO_FIELDS 
                              if data.get(f) not in (None, '') ])
        geocoder = geocoders.GoogleV3()
        try:
            place, (lat, lng) = geocoder.geocode(address.encode('utf8', 'ignore'), exactly_one=False)[0]
            self.point = Point(lng, lat)
        except (TypeError, IndexError, GeocoderQueryError):
            raise forms.ValidationError('Sorry, we were unable to locate this address')
        return data

    def save(self, commit=True):
        instance = super(GeoFormMixin, self).save(commit=False)
        instance.point = self.point
        if commit:
            instance.save()
        return instance