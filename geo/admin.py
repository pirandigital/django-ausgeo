from django.contrib import admin
from django.core.exceptions import ValidationError
from django.forms import ModelForm

from models import Country, Location, City


class ExtendedMetaModelForm(ModelForm):
    """
    Allow the setting of any field attributes via the Meta class.
    Could be remove if this issue is addressed: https://code.djangoproject.com/ticket/13693
    """
    def __init__(self, *args, **kwargs):
        """
        Iterate over fields, set attributes from Meta.field_args.
        """
        super(ExtendedMetaModelForm, self).__init__(*args, **kwargs)
        if hasattr(self.Meta, "field_args"):
            # Look at the field_args Meta class attribute to get
            # any (additional) attributes we should set for a field.
            field_args = self.Meta.field_args
            # Iterate over all fields...
            for fname, field in self.fields.items():
                # Check if we have something for that field in field_args
                fargs = field_args.get(fname)
                if fargs:
                    # Iterate over all attributes for a field that we
                    # have specified in field_args
                    for attr_name, attr_val in fargs.items():
                        if attr_name.startswith("+"):
                            merge_attempt = True
                            attr_name = attr_name[1:]
                        else:
                            merge_attempt = False
                        orig_attr_val = getattr(field, attr_name, None)
                        if orig_attr_val and merge_attempt and\
                           type(orig_attr_val) == dict and\
                           type(attr_val) == dict:
                            # Merge dictionaries together
                            orig_attr_val.update(attr_val)
                        else:
                            # Replace existing attribute
                            setattr(field, attr_name, attr_val)


class CountryForm(ExtendedMetaModelForm):

    class Meta:
        model = Country

        field_args = {
            'name': {
                'error_messages': {
                    'required': 'Please enter the country name'
                }
            },
            'code': {
                'error_messages': {
                    'required': 'Please enter the country code (ex: "au")'
                }
            },
        }


class CountryAdmin(admin.ModelAdmin):
    list_display = ('name', 'code')
    search_fields = ('name', 'code')
    fieldsets = (
        (None, {
           'classes': ('ltgrey', ),
           'fields': (('name', 'code'),)
        }),
    )

    form = CountryForm


class LocationForm(ExtendedMetaModelForm):

    class Meta:
        model = Country

        field_args = {
            'name': {
                'error_messages': {
                    'required': 'Please enter the location name'
                }
            },
            'state': {
                'error_messages': {
                    'required': 'Please select the state'
                }
            },
        }

    def save(self, commit=True):
        location = super(LocationForm, self).save(commit=False)

        try:
            location.country
        except Country.DoesNotExist:
            location.country = Country.objects.get(code="au")

        if commit:
            location.save(commit=True)

        return location

    def clean_point(self):
        message = 'Please make sure this location can be found - click on "Geocode" first'
        point = self.cleaned_data['point']

        if point is None or point == '':
            raise ValidationError(message=message)

        return point


class LocationAdmin(admin.ModelAdmin):
    list_display = ('name', 'state', 'postcode')
    list_filter = ('state',)
    search_fields = ('name', 'state', 'postcode')

    form = LocationForm

    fieldsets = (
        ('Details', {
            'classes': ('ltgrey', ),
            'fields': (
                ('state', 'name', 'postcode'),
                #'city'
            ),
        }),
        ("Location", {
            'classes': ('', ),
            'fields': ('point',),
        }),
    )


class CityAdmin(admin.ModelAdmin):
    list_display = ('name', 'slug')
    search_fields = ('name',)
    fieldsets = (
        (None, {
           'classes': ('ltgrey', ),
           'fields': (('name',), ('location'),)
        }),
    )

    # form = CountryForm

admin.site.register(Country, CountryAdmin)
admin.site.register(Location, LocationAdmin)
admin.site.register(City, CityAdmin)